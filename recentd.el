;;; recentd.el --- Global directory history -*- lexical-binding: t -*-

;; Copyright (C) 2020-2021 Niklas Eklund

;; Author: Niklas Eklund <niklas.eklund@posteo.net>
;; URL: http://gitlab.com/niklaseklund/recentd
;; Version: 0.1
;; Package-Requires: ((emacs "26.3"))
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Recentd provides a history of recently visited directories.  Out of
;; the box it tracks directories when `find-file', `dired' or
;; `shell-cd' is called.  It provides the commands
;; `recentd-dired-change-directory' and
;; `recentd-shell-change-directory' list to quickly switch to a recent
;; directory.

;;;; Credits

;; This package would not have been possible without the following
;; packages: `recentf', which showed me how to implement a recent
;; history.

;;; Code:

;;;; Requirements

(require 'cl-lib)
(require 'shell)
(require 'subr-x)

;;;; Customization

(defgroup recentd nil
  "Global directory history."
  :version "26.3"
  :group 'files)

(defcustom recentd-max-history 200
  "Maximum number of directories stored in `recentd-list'."
  :group 'recentd
  :type 'integer)

(defcustom recentd-file (expand-file-name "recentd" user-emacs-directory)
  "File to store `recentd-list'."
  :group 'recentd
  :type 'string)

;;;; Variables

(defvar recentd-list nil
  "A list of recently visited directories.")
(defvar recentd-history nil
  "The history of selected directories from `recentd-list'.")
(defvar recentd-shell-command nil "The command sent to `shell'.")
(defvar recentd-shell-change-directory-hooks '(recentd-shell-directory-tracker-h)
  "Hooks to run when changing directory in `shell'.")

;;;; Functions

(defun recentd-register ()
  "Add `default-directory' to the top of `recentd-list'.

The function doesn't register remote directories."
  (let ((directory (expand-file-name default-directory)))
    (unless (file-remote-p directory)
      (setq recentd-list
            (thread-last recentd-list
              (seq-remove (lambda (d) (string-equal d directory)))
              (cons directory))))))

(defun recentd-cleanup ()
  "Remove non-existing directories from `recentd-list'."
  (setq recentd-list
        (seq-filter #'file-exists-p recentd-list)))

(defun recentd-load-list ()
  "Load history stored in`recentd-file'."
  (when (file-readable-p recentd-file)
    (setq recentd-list
          (with-temp-buffer
            (insert-file-contents recentd-file)
            (split-string (buffer-string) "\n" t)))
    (recentd-cleanup)))

(defun recentd-save-list ()
  "Save the list of recent directories.
The `recentd-list'saturates at `recentd-max-history' making that the
maximum number of elements."
  (when recentd-list
    (with-temp-file recentd-file
      (insert
       (recentd--serialize-list)))))

;;;; Commands

(defun recentd-dired-change-directory (directory)
  "Change DIRECTORY in `dired'."
  (interactive
   (list (recentd--select-directory)))
  (if (file-exists-p directory)
      (find-file directory)
    (message "Directory %s doesn't exist. Run `recentd-cleanup'." directory)))

(defun recentd-shell-change-directory (directory)
  "Change to DIRECTORY in `shell'."
  (interactive
   (list (recentd--select-directory)))
  (if (file-exists-p directory)
      (cl-letf* ((recentd-shell-command (concat "cd " directory))
                 (comint-input-sender
                  (lambda (proc _string)
                    (comint-simple-send proc recentd-shell-command)))
                 ((symbol-function 'comint-add-to-input-history) (lambda (_) t)))
        (goto-char (point-max))
        (comint-kill-input)
        (comint-send-input)
        (run-hooks 'recentd-shell-change-directory-hooks))
    (message "Directory %s doesn't exist. Run `recentd-cleanup'." directory)))

;;;; Support

(defun recentd-shell-directory-tracker-h ()
  "Hook to run `shell-directory-tracker'."
  (shell-directory-tracker recentd-shell-command))

(defun recentd--serialize-list ()
  "Return a serialized string of `recentd-list'."
  (mapconcat #'concat
             (seq-take recentd-list recentd-max-history) "\n"))

(defun recentd--select-directory ()
  "Select a directory from the `recentd-history'."
  (let* ((candidates recentd-list)
         (metadata `(metadata
                     (category . file)))
         (collection (lambda (string predicate action)
                       (if (eq action 'metadata)
                           metadata
                           (complete-with-action action candidates string predicate)))))
    (completing-read "Select directory: "
                     collection nil t nil 'recentd-history)))

(defun recentd--register-a (orig-fun &rest args)
  "Advice ORIG-FUN with ARGS to call function `recentd-register'."
  (apply orig-fun args)
  (recentd-register))

;;;; Minor mode

(defvar recentd-mode-map (make-sparse-keymap)
  "Keymap to use in recentd mode.")

;;;###autoload
(define-minor-mode recentd-mode
  "Toggle Recentd mode.

When Recentd mode is enabled all the directories that are visited with
`find-file' and `shell-cd' are registered."
  :global t
  :group 'recentd
  :keymap recentd-mode-map

  (if recentd-mode
      (progn
        (recentd-load-list)
        (add-hook 'kill-emacs-hook #'recentd-save-list)
        (add-hook 'find-file #'recentd-register)
        (add-hook 'dired-mode-hook #'recentd-register)
        (advice-add 'shell-cd :around #'recentd--register-a))
    (recentd-save-list)
    (remove-hook 'kill-emacs-hook #'recentd-save-list)
    (remove-hook 'find-file #'recentd-register)
    (remove-hook 'dired-mode-hook #'recentd-register)
    (advice-remove 'shell-cd #'recentd--register-a)))

;;;; Footer

(provide 'recentd)

;;; recentd.el ends here
