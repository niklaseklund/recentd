;;; guix.scm -- Guix package definition

(use-modules
 (guix packages)
 (guix git-download)
 (guix gexp)
 (guix build-system gnu)
 ((guix licenses) #:prefix license:)
 (guix build-system emacs)
 (gnu packages emacs-xyz)
 (ice-9 popen)
 (ice-9 rdelim))

(define %source-dir (dirname (current-filename)))

(define %git-commit
  (read-string (open-pipe "git show HEAD | head -1 | cut -d ' ' -f2" OPEN_READ)))

(define-public emacs-recentd
  (let ((branch "master")
        (commit "9027c75dbbb9f29ffbef8ca59c37ac0769b5d6b7")
        (revision "0"))
    (package
      (name "emacs-recentd")
      (version (git-version "0.1" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://gitlab.com/niklaseklund/recentd")
               (commit commit)))
         (sha256
          (base32
           "0xps22x3zxrfd1smvp9jyfci59msfvmwkqc3wba57bmrnrs6ibgi"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (native-inputs
       `(("emacs-ert-runner" ,emacs-ert-runner)))
      (arguments
       `(#:tests? #t
         #:test-command '("ert-runner")))
      (home-page "https://gitlab.com/niklaseklund/recentd")
      (synopsis "Recent directories.")
      (description "A package which provides a global, persistent, list of recently
visited directories.")
      (license license:gpl3+))))

(package
  (inherit emacs-recentd)
  (name "emacs-recentd-git")
  (version (git-version (package-version emacs-recentd) "HEAD" %git-commit))
  (source (local-file %source-dir
                      #:recursive? #t)))
