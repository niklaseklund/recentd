;;; recentd-test.el --- Tests for recentd.el -*- lexical-binding: t; -*-

;; Copyright (C) 2020-2021  Niklas Eklund

;; Author: Niklas Eklund <niklas.eklund@posteo.net>
;; Url: https://gitlab.com/niklaseklund/recentd
;; Package-requires: ((emacs "26.3"))
;; Version: 0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Tests for recentd.el

;;; Code:

;;;; Requirements

(require 'cl-lib)
(require 'ert)
(require 'recentd)

;;;; Tests

(ert-deftest recentd-test-register ()
  ;; Handle empty list initialization
  (cl-letf ((default-directory "/baz")
            (recentd-list nil))
    (recentd-register)
    (should (equal recentd-list '("/baz"))))

  ;; Add new directory to the list
  (cl-letf ((default-directory "/baz")
            (recentd-list '("/foo" "/bar")))
    (recentd-register)
    (should (equal recentd-list '("/baz" "/foo" "/bar"))))

  ;; Pop existing directory to the top of the list
  (cl-letf ((default-directory "/bar")
            (recentd-list '("/foo" "/bar")))
    (recentd-register)
    (should (equal recentd-list '("/bar" "/foo"))))

  ;; Add new directory that is a parent directory
  (cl-letf ((default-directory "/foo")
            (recentd-list '("/foo/bar")))
    (recentd-register)
    (should (equal recentd-list '("/foo" "/foo/bar"))))

  ;; Don't add remote directory
  (cl-letf ((default-directory "/ssh:remote-machine:/baz")
            (recentd-list '("/foo" "/bar")))
    (recentd-register)
    (should (equal recentd-list '("/foo" "/bar")))))

(ert-deftest recentd-test-cleanup ()
  ;; Remove non-existing directories in `recentd-list'
  (let ((recentd-list `(,default-directory "/foo" "/bar")))
    (recentd-cleanup)
    (should (equal `(,default-directory) recentd-list))))

(ert-deftest recentd-test-load-list ()
  ;; Handle non existent file
  (cl-letf (((symbol-function #'file-readable-p) (lambda (_file) nil))
            (recentd-list nil))
    (recentd-load-list)
    (should (not recentd-list)))

  ;; Initialize list from file
  (cl-letf (((symbol-function #'file-readable-p)
             (lambda (_file) t))
            ((symbol-function #'insert-file-contents)
             (lambda (_file)
               (insert "foo\nbar\nbaz")))
            ((symbol-function #'recentd-cleanup) #'ignore)
            (recentd-list nil))
    (recentd-load-list)
    (should (equal '("foo" "bar" "baz") recentd-list))))

(ert-deftest recentd-test-serialize-list ()
  (let ((recentd-list '("foo" "bar" "baz"))
        (recentd-max-history 2)
        (expected "foo
bar"))
    (should (string= expected (recentd--serialize-list)))))

;;;; Footer

(provide 'recentd-test)

;;; recentd-test.el ends here
